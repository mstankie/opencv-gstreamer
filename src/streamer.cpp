#include <thread>
#include <chrono>
#include <sstream>
#include <opencv2/opencv.hpp>

static const std::string keys {
        "{help h usage ? |      | print this message     }"
        "{cols c         |640   | sample frames width    }"
        "{rows r         |480   | sample frames height   }"
        "{@port          |5000  | TCP port for streaming }"
};

int main(int argc, char** argv )
{
    cv::CommandLineParser parser(argc, argv, keys);
    parser.about(APP_NAME " ver. " APP_VERSION);
    if (parser.has("help")) {
        parser.printMessage();
        return 0;
    }

    int port = parser.get<int>("@port");

    cv::Size imSize(parser.get<int>("cols"), parser.get<int>("rows"));

    // The VideoWriter objects writes subsequent frames into a stream that may be received
    // using the attached index.html file
    std::stringstream pipelineDescription;
    pipelineDescription << "appsrc";
    pipelineDescription << " ! videoconvert";
    pipelineDescription << " ! videoscale ! video/x-raw";
    pipelineDescription << ",width=" << imSize.width;
    pipelineDescription << ",height=" << imSize.height;
    pipelineDescription << " ! theoraenc";
    pipelineDescription << " ! oggmux";
    pipelineDescription << " ! tcpserversink host=0.0.0.0 port=" << port;

    cv::VideoWriter out(pipelineDescription.str(),
                        cv::CAP_GSTREAMER,
                        0,
                        30,
                        imSize,
                        true);

    if(!out.isOpened())
    {
        std::cout << "VideoWriter not opened" << std::endl;
        exit(-1);
    }

    size_t imageCounter = 0;

    std::cout << "Streaming at port 5000" << std::endl;

    const cv::Size flyingBoxSize(10, 10);
    const int flyingBoxY = (imSize.height - flyingBoxSize.height) / 2;

    while (true) {

        cv::Mat frame = cv::Mat::zeros(imSize, CV_8UC3);

        cv::putText(frame, std::to_string(imageCounter++),
                    cv::Point(20, 30), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(255, 255, 255));

        int flyingBoxX = imageCounter % (imSize.width - flyingBoxSize.width);
        cv::Rect flyingBox(cv::Point(flyingBoxX, flyingBoxY), flyingBoxSize);

        frame(flyingBox).setTo(cv::Scalar(255, 0, 0));

        out.write(frame);
        
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

    }

    return 0;
}