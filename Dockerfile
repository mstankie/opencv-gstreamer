FROM opencv-gstreamer-build:latest AS build

LABEL description="Uses appropriate buildsystem to build this streaming app \
      and then copies the compiled executable and libraries to new, runtime \
      container which may be run as streaming server based on the app."

WORKDIR /src

COPY CMakeLists.txt ./
COPY src/streamer.cpp src/

RUN cmake . && make

FROM ubuntu:19.10

RUN apt-get update && \ 
    apt-get install -y libjpeg8-dev libtiff5-dev && \
    apt-get install -y libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev

COPY --from=build /usr/local/lib/libopencv* /usr/local/lib/
RUN ldconfig

WORKDIR /opt/OpenCV-GStreamer

COPY --from=build /src/OpenCV-GStreamer ./

ENTRYPOINT ["./OpenCV-GStreamer"]
