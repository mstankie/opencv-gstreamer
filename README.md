OpenCV streaming application with GStreamer
===========================================

Functions and classes in [OpenCV][1] library typically do much more than just
what its names may suggest.

One of such powerful tool is OpenCV's [VideoWriter class][2].
It may not only help you generate a movie file out of images your algorithm
produces, but combined with properly configured [GStreamer][3] it may turn
your OpenCV-based application into a streaming service.

This feature may be used for building headless image processing sensors based
on mini computers like RaspberryPi, Nvidia Jetson (and may more).
Live image view is required for configuration and may be an important part of
configuration web interfaces for such devices.


Project
-------

This projects is a full example of how to build, configure and run
image streaming application based on OpenCV library using GStreamer.

It provides a very basic HTML5 sample to show the receiver web component
compatible with such image streaming server.

It uses [Docker][4] images to ensure all codecs and GStreamer plugins are
available and OpenCV is configured properly.

The sample server streams a 640x480 frames with image counter displayed in the
top-left corner and a 10x10 pixel blue square moving left to right.


Motivation
----------

There is a number of discussions in the Internet (StackOverflow,
OpenCV's Answers) on how to configure `cv::VideoWriter` to stream images
to web interface.
The correct configuration depends on many things like operating system used
or even version of the OS (as it may contain different GStreamer plugins),
the version of OpenCV and the way one compiles it.

That's why one can demonstrate a robust working sample only with accompanying
build and runtime environments.

Docker containers seem to be a perfect solution to ensure build and run
environments have all the components required.


Usage
-----

This repository is a self-documented sample so that you can reproduce
this step-by-step for your application on your PC with your configuration.

Dockerfiles show how to configure your build- and run- environments so that
your streaming app will work.

The sample C++ code shows how to use the `cv::VideoWriter` for streaming apps.
In the first instance try to implement your solution based on my samples.

The HTML sample shows how to implement the receiver widget
in your web interface.


Running the sample
------------------

1. [Install docker](https://docs.docker.com/install/)
2. Build buildsystem image[^1]
    - `docker build --rm -f docker/opencv-gstreamer-build/Dockerfile -t opencv-gstreamer-build:latest`
3. Compile the application in runtime container
    - `docker build --rm . -t opencv-gstreamer:latest`
4. Run the streaming application
    - `docker run --rm -it --name streamer -p 5000:5000 opencv-gstreamer:latest`
5. See the stream in the browser
    - open `webintf/index.html` in your browser
6. Stop the streaming application
    - `docker stop opencv-gstreamer`


Contributing
------------

If you see a bug or a way to improve the project, please open an issue.

I would be especially happy to add more documentation on supported GStreamer
pipeline description features and how those could improve the application.


License
-------

This code is shared under [MIT License](https://opensource.org/licenses/MIT).




[1]: https://opencv.org
[2]: https://docs.opencv.org/4.2.0/dd/d9e/classcv_1_1VideoWriter.html
[3]: https://gstreamer.freedesktop.org/
[4]: https://www.docker.com/

[^1]: https://docs.docker.com/engine/reference/commandline/build/